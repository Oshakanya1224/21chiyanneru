<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<html>
    <head>
        <meta charset="utf-8">
        <title>21ちゃんねる</title>

		<!-- JavaScript手順③：JavaScriptでバリデーション処理を追加 -->
		<!-- JavaScriptはheadタグ内に記述 -->
		<script type="text/javascript">
			function commentCheck(){
	            if (commentForm.comment.value == ""){

	                //条件に一致する場合(コメントが空の場合)
	                alert("コメントを入力してください");

	              	//送信ボタン本来の動作をキャンセル
	                return false;
	            } else if (commentForm.comment.value.length >= 30){

	            	//条件に一致する場合(コメントが30文字以上の場合)
	                alert("コメントは30文字以下で入力してください");

	              	//送信ボタンの動作をキャンセル
	                return false;
	            }else{

	                //条件に一致しない場合(投稿が入力されている場合)
	                //送信ボタンの動作を実行
	                return true;
	            }
	        }

	        // 投稿＆コメント消去実行コード
			function Check(){
		    var word;
		    word = prompt("合言葉は? ", "");
		    	if(word == "kana"){
		    		return true;
		    	} else {
			    	window.alert("合言葉が違います");
					return false;
		    	}
			}
	  	</script>
    </head>
    <body>
        <h1>${hello}</h1>
        <!-- hrefにはURL書く(jspではない) -->
        <a href="insert">新規投稿しちゃう？！</a>

        <!-- バリデーション -->
		<h2>
	       <c:out value="${error3}"></c:out>
	    </h2>

	    <p>
	       <c:out value="${error4}"></c:out><br />
	    </p><br />

	    <p>
	       <c:out value="${error5}"></c:out><br />
	    </p><br />

		<!-- 投稿表示 -->
        <!-- messagesはControllerと対応、ここではmessageとして扱う -->
        <c:forEach items="${messages}" var="message">

           	<!-- 左が上記varのmessageと対応、右がsetter.getterと対応 -->
           	<!-- messagesのisStoppedが0なら投稿表示 -->
			<c:if test="${message.isStopped == 0}">
           		<p><c:out value="${message.message}"></c:out></p>
           	</c:if>

           	<!-- 投稿削除 -->

           	<!-- messageFormでController紐づけ -->
    		<form:form modelAttribute="messageForm">

    			<!-- submit押されたときにhiddenでmessagesテーブルのidをmessageIdという変数に入れて送る -->
           		<!-- submit押されたときにhiddenで1をisStoppedという変数に入れて送る -->
	         	<input type="hidden" name="id" value="${message.id}">
	         	<input type="hidden" name="isStopped" value="1">

	         	<!-- messagesテーブルのisStoppedが0なら消去ボタン表示 -->
				<c:if test="${message.isStopped == 0}">
	           		<input type="submit" value="投稿消しちゃう？" name="messageDelete" onClick="return Check();"/><br /><br />
				</c:if>
			</form:form>
			<c:if test = "${message.isStopped == 1}">
	   			<p><c:out value="この投稿は管理者によって削除されました"></c:out></p><br />
	   		</c:if>

	        <!-- コメント表示 -->
	       	<c:forEach items="${comments}" var="comment">

	       		<!-- 投稿とコメント結びつけ -->
				<c:if test="${comment.messageId == message.id}">

					<!-- messagesのisStoppedが0ならコメント表示 -->
				    <c:if test="${message.isStopped == 0}">

			         	<!-- commentsテーブルのisStoppedが0ならコメント表示 -->
						<c:if test="${comment.isStopped == 0}">
			           		<!-- 左が上記varのcommentと対応、右がsetter.getterと対応 -->
			           		<p><c:out value="${comment.comment}"></c:out></p>
			           	</c:if>
			        </c:if>

			       	<!-- コメント削除 -->

			       	<!-- commentFormでController紐づけ -->
		    		<form:form modelAttribute="commentForm">

		    			<!-- submit押されたときにhiddenでcommentsテーブルのidをidという変数に入れて送る -->
		           		<!-- submit押されたときにhiddenで1をisStoppedという変数に入れて送る -->
			         	<input type="hidden" name="id" value="${comment.id}">
			         	<input type="hidden" name="isStopped" value="1">

				       	<!-- messagesのisStoppedが0なら消去ボタン表示 -->
				       	<c:if test="${message.isStopped == 0}">

				       		<!-- commentsテーブルのisStoppedが0ならコメント表示 -->
							<c:if test="${comment.isStopped == 0}">
								<input type="submit" value="コメント消しちゃう？" name="commentDelete" onClick="return Check();"/><br /><br />
							</c:if>
						</c:if>

					</form:form>
					<c:if test = "${comment.isStopped == 1}">
			   			<p><c:out value="このコメントは管理者によって削除されました"></c:out></p><br />
			   		</c:if>
			   	</c:if>
		   	</c:forEach>

			<!-- コメント入力 -->
			<!-- commentFormでController紐づけ -->
			<!-- JavaScript手順②：form特定の目的でnameの付け足し -->
		    <form:form modelAttribute="commentForm" name="commentForm">

            	<!-- submit押されたときにhiddenでmessageテーブルのidをmessageIdという変数に入れて送る -->
          		<input type="hidden" name="messageId" value="${message.id}">

          		<!-- 投稿が消されていたらコメント欄も表示しない(messageのisStoppedが0ならコメント欄表示) -->
          		<c:if test="${message.isStopped == 0}">
	          		<form:textarea path="comment" cols="30" rows="5"/><br /><br />

	          		<!-- JavaScript手順①：onClic関数追記 -->
	          		<input type="submit" value="コメントしちゃうヨ" onClick="return commentCheck();"><br /><br />
	          	</c:if>
       		</form:form>
        </c:forEach>
    </body>
</html>