<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>新規投稿画面</title>

		<!-- JavaScript手順③：JavaScriptでバリデーション処理を追加 -->
		<!-- JavaScriptはheadタグ内に記述 -->
	    <script type="text/javascript">
	        function check(){
	            if (messageForm.message.value == ""){

	                //条件に一致する場合(投稿が空の場合)
	                alert("投稿を入力してください");

	              	//送信ボタン本来の動作をキャンセル
	                return false;
	            } else if (messageForm.message.value.length >= 50){

	            	//条件に一致する場合(投稿が50文字以上の場合)
	                alert("投稿は50文字以下で入力してください");

	              	//送信ボタンの動作をキャンセル
	                return false;
	            }else{

	                //条件に一致しない場合(投稿が入力されている場合)
	                //送信ボタンの動作を実行
	                return true;
	            }
	        }
	    </script>
	</head>
	<body>
		<h1>${hello1}</h1>
		<h2>${hello2}</h2>

		<!-- バリデーション -->
		<h2>
	       <c:out value="${error1}"></c:out>
	    </h2>

	    <p>
	       <c:out value="${error2}"></c:out><br />
	    </p>

		<!-- messageFormでController紐づけ -->
		<!-- JavaScript手順②：form特定の目的でnameの付け足し -->
	    <form:form modelAttribute="messageForm" name="messageForm">

	    	<!-- バリデーション用form:errorsタグの追加 -->
		    <!-- path=以降はFormと対応 -->
	    	<div><form:errors path="message"  /></div>

	    	<!-- pathはformのsetter,getterと対応 -->
	        <form:textarea path="message" cols="50" rows="5"/><br /><br />

	        <!-- JavaScript手順①：onClic関数追記 -->
	        <input type="submit" value="投稿しちゃうヨ" onClick="return check();">
	    </form:form>
	</body>
</html>