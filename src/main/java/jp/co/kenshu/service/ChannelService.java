package jp.co.kenshu.service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import jp.co.kenshu.dto.CommentDto;
import jp.co.kenshu.dto.MessageDto;
import jp.co.kenshu.entity.CommentEntity;
import jp.co.kenshu.entity.MessageEntity;
import jp.co.kenshu.mapper.CommentMapper;
import jp.co.kenshu.mapper.MessageMapper;

@Service
public class ChannelService {

	//投稿一件取得
    @Autowired
    private MessageMapper messageMapper;

    public MessageDto getMessage(Integer id) {
        MessageDto dto = new MessageDto();
        MessageEntity entity = messageMapper.getMessage(id);
        BeanUtils.copyProperties(entity, dto);
        return dto;
    }

    //投稿全件取得
    public List<MessageDto> getMessageAll() {
        List<MessageEntity> messageList = messageMapper.getMessageAll();
        List<MessageDto> resultList = convertToDto(messageList);
        return resultList;
    }

    private List<MessageDto> convertToDto(List<MessageEntity> messageList) {
        List<MessageDto> resultList = new LinkedList<>();
        for (MessageEntity entity : messageList) {
            MessageDto dto = new MessageDto();
            BeanUtils.copyProperties(entity, dto);
            resultList.add(dto);
        }
        return resultList;
    }

    //投稿をデータベースに追加
    public int insertMessage(String message, Date createdDate, Date updatedDate) {
        int count = messageMapper.insertMessage(message, createdDate, updatedDate);
        return count;
    }

    //コメントをデータベースに追加
    public int insertComment(String comment, int messageId, Date createdDate, Date updatedDate) {
        int count1 = commentMapper.insertComment(comment, messageId, createdDate, updatedDate);
        return count1;
    }

    //コメント投稿と同時にmessagesのupdated_dateも更新したいので、
    //コメントをデータベースに送ると同時にmessageIdを送る
    public int updateMessage(int messageId) {
        int count2 = commentMapper.updateMessage(messageId);
        return count2;
    }

    //コメント全件取得
  	@Autowired
      private CommentMapper commentMapper;

      public List<CommentDto> getCommentAll() {
          List<CommentEntity> commentList = commentMapper.getCommentAll();
          List<CommentDto> resultList = convertToCommentDto(commentList);
          return resultList;
      }

      private List<CommentDto> convertToCommentDto(List<CommentEntity> commentList) {
          List<CommentDto> resultList = new LinkedList<>();
          for (CommentEntity entity : commentList) {
              CommentDto dto = new CommentDto();
              BeanUtils.copyProperties(entity, dto);
              resultList.add(dto);
          }
          return resultList;
      }

      //投稿削除
      //jsp,controllerで送られてきたidとisStopped=1をmessagesテーブルに挿入
      public int deleteMessage(int id, int isStopped) {
          int count = messageMapper.deleteMessage(id, isStopped);
          return count;
      }

      //コメント削除
      //jsp,controllerで送られてきたidとisStopped=1をcommentsテーブルに挿入
      public int deleteComment(int id, int isStopped) {
          int count = commentMapper.deleteComment(id, isStopped);
          return count;
      }
}