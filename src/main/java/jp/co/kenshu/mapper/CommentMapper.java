package jp.co.kenshu.mapper;

import java.util.Date;
import java.util.List;

import org.apache.ibatis.annotations.Param;

import jp.co.kenshu.entity.CommentEntity;

public interface CommentMapper {

	//コメント全件取得
    List<CommentEntity> getCommentAll();

    //コメントをデータベースに追加
    //引数複数の時Param使う必要ある
    int insertComment(@Param("comment")String comment, @Param("messageId")int messageId, @Param("createdDate")Date createdDate, @Param("updatedDate")Date updatedDate);

    //コメント削除
    //serviceで送られてきたcommentIdとisStopped=1をcommentsテーブルに挿入
    int deleteComment(@Param("id")int id, @Param("isStopped")int isStopped);

    //コメント投稿と同時にmessagesのupdated_dateも更新したいので、
    //コメントをデータベースに送ると同時にmessageIdを送る
    int updateMessage(int messageId);
}
