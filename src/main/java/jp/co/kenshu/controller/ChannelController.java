package jp.co.kenshu.controller;

import java.util.List;

import javax.validation.Valid;

import org.jboss.logging.Logger;
import org.jboss.logging.Logger.Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import jp.co.kenshu.dto.CommentDto;
import jp.co.kenshu.dto.MessageDto;
import jp.co.kenshu.form.CommentForm;
import jp.co.kenshu.form.MessageForm;
import jp.co.kenshu.service.ChannelService;

@Controller
public class ChannelController {

	//一件取得
	//http://localhost:8080/21ch/1

    @Autowired
    private ChannelService channelService;

    //value = "/21ch/"はconfigと連動
    @RequestMapping(value = "/{id}", method = RequestMethod.GET)
    public String message(Model model, @PathVariable int id) {
        MessageDto message = channelService.getMessage(id);

        //ダブルコーテーション内はjspと対応
        model.addAttribute("hello", "ようこそ21ちゃんねるへ");
        model.addAttribute("message", message);
        return "message";//message.jspの表示
    }

    //全件取得
    //http://localhost:8080/21ch/home
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public String messageAll(Model model) {

    	//投稿全件取得
        List<MessageDto> messages = channelService.getMessageAll();
        model.addAttribute("hello", "ようこそ21ちゃんねるへ！");
        model.addAttribute("messages", messages);

        //コメント全件取得
        List<CommentDto> comments = channelService.getCommentAll();
        model.addAttribute("comments", comments);

        //jspのコメント入力欄を表示するためにcommentFormで紐づけ
        //表示なので、URLがhomeの時のGETを動かすためにここに書く
        CommentForm commentForm = new CommentForm();
        model.addAttribute("commentForm", commentForm);

        return "home";//home.jspの表示
    }

    //コメントをデータベースに追加
    @RequestMapping(value = "/home", method = RequestMethod.POST)
    public String commentInsert(@Valid @ModelAttribute CommentForm commentForm, BindingResult result, Model model) {

    	//Formのバリデーションに引っかかった場合
    	if (result.hasErrors()) {

    		//エラー表示するとホーム画面の内容全部消えるので、
    		//Getにある表示に必要なもの全部ここにコピーしてくる
    		//投稿全件取得
            List<MessageDto> messages = channelService.getMessageAll();
            model.addAttribute("hello", "ようこそ21ちゃんねるへ！");
            model.addAttribute("messages", messages);

            //コメント全件取得
            List<CommentDto> comments = channelService.getCommentAll();
            model.addAttribute("comments", comments);

            //エラー表示用
	        model.addAttribute("error3", "エラー");
	        model.addAttribute("error4", "以下のエラーを解消してください");
	        model.addAttribute("error5", "コメントを30文字以下で入力してください");

	        return "home";//エラーメッセージをhome.jspに表示

	    //バリデーションに引っかからなかった場合
    	} else {

	    	//コメントをデータベースに追加
	        int count1 = channelService.insertComment(commentForm.getComment(), commentForm.getMessageId(),
	        		commentForm.getCreatedDate(), commentForm.getUpdatedDate());
	        Logger.getLogger(ChannelController.class).log(Level.INFO, "挿入件数は" + count1 + "件です。");

	        //コメント投稿した時に、同時に投稿の更新日時を更新(update)したいので、
	        //コメント押した後hiddenで送られるmessageIdをキーにmessagesのupdated_dateを更新
	        int count2 = channelService.updateMessage(commentForm.getMessageId());
	        Logger.getLogger(ChannelController.class).log(Level.INFO, "更新件数は" + count2 + "件です。");

	        return "redirect:/home";//投稿画面で投稿後、ホーム画面へ遷移
	    }
    }

    //投稿をデータベースに追加
    //http://localhost:8080/21ch/insert

    //投稿画面を表示
    @RequestMapping(value = "/insert", method = RequestMethod.GET)
    public String testInsert(Model model) {

    	//jspの投稿入力欄を表示するためにmessageFormで紐づけ
        MessageForm messageForm = new MessageForm();

        //左のmessageFormはjsp,右は上記と対応
        model.addAttribute("messageForm", messageForm);
        model.addAttribute("hello1", "投稿を入力してちょ");
        model.addAttribute("hello2", "どしどし投稿してちょ");
        return "newMessage";//newMessage.jspを表示
    }

    //投稿した後の処理
    @RequestMapping(value = "/insert", method = RequestMethod.POST)
    public String messageInsert(@Valid @ModelAttribute MessageForm messageForm, BindingResult result, Model model) {

    	//Formのバリデーションに引っかかった場合
    	if (result.hasErrors()) {

    		//エラー表示した時「投稿入力してちょ」たちが消えてしまうので追加
    		model.addAttribute("hello1", "投稿を入力してちょ");
            model.addAttribute("hello2", "どしどし投稿してちょ");

            //エラー表示用
	        model.addAttribute("error1", "エラー");
	        model.addAttribute("error2", "以下のエラーを解消してください");
	        return "newMessage";//エラーメッセージをnewMessage.jspに表示

	    //バリデーションに引っかからなかった場合
    	} else {
	    	int count = channelService.insertMessage(messageForm.getMessage(), messageForm.getCreatedDate(), messageForm.getUpdatedDate());
	    	Logger.getLogger(ChannelController.class).log(Level.INFO, "挿入件数は" + count + "件です。");
	    	return "redirect:/home";//通常通り投稿後、ホーム画面へ遷移
	    }
    }

    //投稿削除
    //deleteって書いてあるけど実際はjspで送られてきたmessageIdとisStopped=1をmessagesテーブルに挿入
    //同じURL,POSTの処理でもparamsを追加したら記述できる
    @RequestMapping(value = "/home", params="messageDelete", method = RequestMethod.POST)
    public String messageDelete(@ModelAttribute MessageForm messageForm, Model model) {

        int count = channelService.deleteMessage(messageForm.getId(), messageForm.getIsStopped());
        Logger.getLogger(ChannelController.class).log(Level.INFO, "削除件数は" + count + "件です。");

        return "redirect:/home";//投稿画面で投稿後、ホーム画面へ遷移
    }

    //コメント削除
    //deleteって書いてあるけど実際はjspで送られてきたidとisStopped=1をcommentsテーブルに挿入
    //同じURL,POSTの処理でもparamsを追加したら記述できる
    @RequestMapping(value = "/home", params="commentDelete", method = RequestMethod.POST)
    public String commentDelete(@ModelAttribute CommentForm commentForm, Model model) {

        int count = channelService.deleteComment(commentForm.getId(), commentForm.getIsStopped());
        Logger.getLogger(ChannelController.class).log(Level.INFO, "削除件数は" + count + "件です。");

        return "redirect:/home";//投稿画面で投稿後、ホーム画面へ遷移
    }
}