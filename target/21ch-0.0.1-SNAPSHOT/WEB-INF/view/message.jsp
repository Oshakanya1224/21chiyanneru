<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<html>
    <head>
        <meta charset="utf-8">
        <title>Welcome</title>
    </head>
    <body>
        <h1>${hello}</h1>

        <!-- 左がControllerと対応、右がsetter.getterと対応 -->
        <h2>${message.message}</h2>
    </body>
</html>