<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規投稿画面</title>
</head>
<body>
	<h1>${hello1}</h1>
	<h2>${hello2}</h2>

	<!-- messageFormでController紐づけ -->
    <form:form modelAttribute="messageForm">

    	<!-- pathはformのsetter,getterと対応 -->
        <form:textarea path="message" cols="50" rows="5"/><br /><br />
        <input type="submit" value="投稿しちゃうヨ">
    </form:form>
</body>
</html>